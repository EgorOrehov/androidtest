package com.example.testapp.adapters;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.testapp.R;
import com.example.testapp.db.DataItem;

import java.util.ArrayList;
import java.util.List;


public class ListViewAdapter extends ArrayAdapter<String> {


    private final Activity activity;
    private List<DataItem> items = new ArrayList<>();

    public ListViewAdapter(Activity activity, int textViewResourceId) {
        super(activity, textViewResourceId);
        this.activity = activity;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    private View getCustomView(int position, ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();
        View row = inflater.inflate(R.layout.list_item_layout, parent, false);

        TextView parameter_name = row.findViewById(R.id.parameter_name);
        TextView parameter_value = row.findViewById(R.id.parameter_value);

        parameter_name.setText(String.valueOf(items.get(position).position));
        parameter_value.setText(items.get(position).name);

        return row;
    }

    public void updateData(@NonNull List<DataItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public List<DataItem> getItems() {
        return items;
    }
}