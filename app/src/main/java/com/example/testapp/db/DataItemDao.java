package com.example.testapp.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DataItemDao {

    @Query("SELECT * FROM DataItem")
    Flowable<List<DataItem>> getAll();

    @Query("SELECT * FROM DataItem WHERE id = :id")
    DataItem getById(long id);

    @Insert(onConflict = REPLACE)
    Long insert(DataItem countryModel);

    @Delete
    int delete(DataItem dataItem);

    @Update
    int update(DataItem dataItem);
}
