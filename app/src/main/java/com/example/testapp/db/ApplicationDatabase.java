package com.example.testapp.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {DataItem.class}, version = 1)
public abstract class ApplicationDatabase extends RoomDatabase {
    public abstract DataItemDao dataItemDao();
}