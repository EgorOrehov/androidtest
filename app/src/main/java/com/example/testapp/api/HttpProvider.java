package com.example.testapp.api;

import androidx.annotation.NonNull;

import okhttp3.OkHttpClient;

public final class HttpProvider {

    private HttpProvider() {}

    private static final OkHttpClient client = buildClient();

    @NonNull
    public static OkHttpClient provideClient() {
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.create())
                .addInterceptor(ApiInterceptor.create())
                .build();
    }

}
