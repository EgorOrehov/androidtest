package com.example.testapp.api;

import io.reactivex.Observable;
import retrofit2.http.GET;


public interface Api {

    @GET("/planetary/apod")
    Observable<ResponseDataClass> getData();

}
