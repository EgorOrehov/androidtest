package com.example.testapp.api;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;


public class ResponseDataClass {

    @SerializedName("url")
    private String mUrl;

    @NonNull
    public String getUrl() {
        return mUrl;
    }
}
